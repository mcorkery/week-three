fil = new Array();
fil["0"]= "topics/concept_topics/Aikido.html@@@Aikido, The Gentle Martial Art@@@Aikido is a Japanese martial art developed by Morihei Ueshiba. On a purely physical level it is an art involving throws and joint locks that are derived from Jujitsu and some throws and other techniques derived from Kenjutsu...";
fil["1"]= "topics/concept_topics/chapter-intro.html@@@Aikido, The Gentle Martial Art@@@Aikido is a Japanese martial art developed by Morihei Ueshiba. On a purely physical level it is an art involving throws and joint locks that are derived from Jujitsu and some throws and other techniques derived from Kenjutsu...";
fil["2"]= "topics/concept_topics/rokyu-requirements.html@@@Rokyu (6th Kyu) Requirements@@@6th Kyu is the first level of white belt earned by Aikido beginners...";
fil["3"]= "topics/glossentry/gloss-tenkai.html@@@tankai@@@...";
fil["4"]= "topics/reference_topics/Ref_1.html@@@Shomenuchi ikkyo@@@First principle ikkyo for Shomenuchi attack...";
fil["5"]= "topics/reference_topics/ref_2.html@@@Shomenuchi iriminage@@@Entering throw iriminage for Shomenuchi attack...";
fil["6"]= "topics/reference_topics/ref_3.html@@@Munetsuki Kotegaeshi@@@First principle ikkyo for Shomenuchi attack...";
